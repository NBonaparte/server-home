lastversion() {
    basename $(curl -fs -o/dev/null -w %{redirect_url} $1/releases/latest)
}

URL="https://github.com/be5invis/iosevka"
ver=$(lastversion $URL)
mkdir tmp
cd tmp
curl -L -o iosevka.zip $URL/releases/download/$ver/ttf-iosevka-${ver:1}.zip
unzip iosevka.zip
cd ..
cp tmp/*.css $1
cp -r tmp/woff2 $1
rm -r tmp
